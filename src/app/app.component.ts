import { Component } from '@angular/core';
import { CryptoAPIService } from './crypto-api.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'angular-cryptocurrency';
  values: any = [];


  constructor(private cryptoAPIService: CryptoAPIService) { }

  ngOnInit() {
    this.get50Cryptos();
}
  get50Cryptos(){
    return this.cryptoAPIService.getCryptos().subscribe(res => {

      for (var val of res) {
        this.values = res;
      }
     });

   }

}
