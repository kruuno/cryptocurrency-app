import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class CryptoAPIService {

  constructor(private http: HttpClient) { }

  url: string = "https://api.coingecko.com/api/v3/coins/markets?vs_currency=usd&order=market_cap_desc&per_page=50&page=1&sparkline=false";

  getCryptos() {
    return this.http.get(this.url).
    pipe(
    map(data => Object.keys(data).map(k => data[k]))
    );
  }

  getChartValues(id: any, period: any) {
    return this.http.get("https://api.coingecko.com/api/v3/coins/"+id+"/market_chart?vs_currency=usd&days="+period).
    pipe(
    map(data => Object.keys(data).map(k => data[k]))
    );
  }

  getCoinById(id: any) {
    return this.http.get("https://api.coingecko.com/api/v3/coins/"+id).
    pipe();
  }



}
