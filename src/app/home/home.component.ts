import { Component, OnInit } from '@angular/core';
import { CryptoAPIService } from '../crypto-api.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  values: any = [];
  constructor(private cryptoAPIService: CryptoAPIService) { }

  ngOnInit(): void {
    this.get50Cryptos();
  }

  get50Cryptos(){
    return this.cryptoAPIService.getCryptos().subscribe(res => {
      for (var val of res) {
        this.values = res;
      }
  });
}

numberWithCommas(x) {
  return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}

}
