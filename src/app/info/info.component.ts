import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { CryptoAPIService } from '../crypto-api.service';
import Chart from 'chart.js';
import * as $ from 'jquery';

@Component({
  selector: 'app-info',
  templateUrl: './info.component.html',
  styleUrls: ['./info.component.scss']
})
export class InfoComponent implements OnInit {

  public chart: Chart = [];
  id: any;
  date = [];
  price = [];
  activeState: string = '24h';
  currentPrice;
  imageLink;
  priceChange24h;
  marketCap;
  marketCapRank;
  websiteLink;

  constructor(private cryptoAPIService: CryptoAPIService, private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.id = this.route.snapshot.paramMap.get('id');
    console.log(this.id);
    this.getValueById(1);
    this.getCoin();
  }

  changeStatus(activeState){
    this.activeState = activeState;
  }

  updateChart(period){
    let total = this.chart.data.labels.length;
    $("canvas#canvas").remove();
    $("div.chart").append('<canvas id="canvas">{{ chart }}</canvas>');
    while (total >= 0) {
        this.chart.data.labels.pop();
        this.chart.data.datasets[0].data.pop();
        total--;
    }
    this.chart.update();
    this.getValueById(period);
  }

  getValueById(period) {
    console.log(this.chart);
    return this.cryptoAPIService.getChartValues(this.id, period).subscribe(res => {
      for(let val of res[0]) {
        var temp = new Date(val[0]).toLocaleTimeString('en', { year: 'numeric', month: 'short', day: 'numeric', hour12: false, hour: '2-digit', minute: '2-digit' });
        this.date.push(temp);
        val[1] = Math.round((val[1] + Number.EPSILON) * 100) / 100; // round to 2 decimals
        this.price.push(val[1]);
      }
      this.chart = new Chart('canvas', {
        type: 'line',
        data: {
          labels: this.date,
          datasets: [
            {
              data: this.price,
              borderColor: '#45b6fe'
            },
          ]
        },
        options: {
          tooltips: {
            callbacks: {
              label: (tooltipItems, data) => {
                  return '$' + data.datasets[tooltipItems.datasetIndex].data[tooltipItems.index];
              }
          },
        },
          legend: {
            display: false
          },
          scales: {
            xAxes: [{
              display: true
            }],
            yAxes: [{
              ticks: {
                userCallback: function(tick) {
                  return '$' + tick.toString() ;
                }
              },
              display: true
            }],
          }
        }
      });
      console.log("Chart created");
    });
  }

  numberWithCommas(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
  }

  getCoin() {
    return this.cryptoAPIService.getCoinById(this.id).subscribe(res => {
      console.log(res);
      this.websiteLink = res['links'].homepage[0];
      this.priceChange24h = res['market_data'].price_change_percentage_24h.toFixed(2);
      this.imageLink = res['image'].small;
      this.marketCapRank = res['market_cap_rank'];
      this.marketCap = this.numberWithCommas(res['market_data'].market_cap.usd);
      this.currentPrice = this.numberWithCommas(res['market_data'].current_price.usd);
  });
  }

}
